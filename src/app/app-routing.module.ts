import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageNotFoundComponent } from './not-found.component';
import { Question1Component } from './question1/question1.component';
import { Question2Component } from './question2/question2.component';
import { Question4Component } from './question4/question4.component';
import { BonusQuestionComponent } from './bonus-question/bonus-question.component';

const appRoutes: Routes = [
  { path: '',   redirectTo: '/question1', pathMatch: 'full'},
  { path: 'question1', component: Question1Component},
  { path: 'question2', component: Question2Component},
  { path: 'question4', component: Question4Component},
  { path: 'bonusquestion', component: BonusQuestionComponent},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      //{ enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
