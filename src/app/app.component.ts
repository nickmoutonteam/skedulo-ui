import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { GithubApiService } from './github-api.service';
import { Subscription } from 'rxjs';
import { forwardRef } from '@angular/core';
import { debounceTime } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material';
import * as _ from "lodash";

@Component({
  selector: 'ghu-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

}
