import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ReactiveFormsModule }   from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import { AppComponent } from './app.component';
import { GithubApiService } from './github-api.service';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AppRoutingModule } from './app-routing.module';
import { Question1Component } from './question1/question1.component';
import { Question2Component } from './question2/question2.component';
import { Question4Component } from './question4/question4.component';
import { BonusQuestionComponent } from './bonus-question/bonus-question.component';
import { PageNotFoundComponent } from './not-found.component';
import { Router } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    Question1Component,
    Question2Component,
    Question4Component,
    BonusQuestionComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatInputModule,
    MatTableModule,
    MatProgressSpinnerModule,
    AppRoutingModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule
  ],
  providers: [GithubApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
