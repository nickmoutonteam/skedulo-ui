import { Component, OnInit } from '@angular/core';
import { SkeduloApiService } from '../skedulo-api.service';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'ghu-bonus-question',
  templateUrl: './bonus-question.component.html',
  styleUrls: ['./bonus-question.component.scss']
})
export class BonusQuestionComponent implements OnInit {
  loading: boolean = false;
  bonusQuestionSub: Subscription;
  displayedColumns = ['window', 'band'];
  schedule: any;

  constructor(private skeduloApiService: SkeduloApiService) {
    this.bonusQuestionSub = this.skeduloApiService.bonusQuestionResponseRx()
      .subscribe(response => {
        if(response){
          this.loadPage(response)
        }
    });
  }

  loadPage(response){
    this.loading = false;
    console.log(response)
    this.schedule = response;
    console.log(this.schedule)
  }

  getBonusAnswer(){
    this.loading = true;
    this.skeduloApiService.bonusQuestion();
  }

  ngOnInit() {
  }

}
