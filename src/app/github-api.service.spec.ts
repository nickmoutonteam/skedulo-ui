import { TestBed, inject } from '@angular/core/testing';
import { GithubApiService } from './github-api.service';
import { HttpClientModule }    from '@angular/common/http';

describe('GithubApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [GithubApiService]
    });
  });

  it('should be created', inject([GithubApiService], (service: GithubApiService) => {
    expect(service).toBeTruthy();
  }));
});
