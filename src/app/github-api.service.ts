import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient,HttpParams,HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GithubApiService {
  private gitHubUserSearchUrl = environment.baseGithubUrl + '/search/users';
  private gitHubSource = new Subject<any>();

  constructor(private http: HttpClient) {
  }

  gitHubResponseRx(): Observable<any> {
    return this.gitHubSource.asObservable();
  }

  searchUsers(username){

    const params = new HttpParams()
      .set('q', username)
      .set('page','1')
      .set('per_page','100');

    return this.http
      .get(this.gitHubUserSearchUrl,{params})
      .subscribe(resp => {
        this.gitHubSource.next(resp);
      },err => {
        if(err instanceof HttpErrorResponse) {
          if (err.status === 403) {
            this.gitHubSource.next("chill");
          }
        }
      });
  }

}
