import { Component, OnInit } from '@angular/core';
import { SkeduloApiService } from '../skedulo-api.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ghu-question1',
  templateUrl: './question1.component.html',
  styleUrls: ['./question1.component.scss']
})
export class Question1Component {
  question1Sub: Subscription;
  loading: boolean = false;
  numbers: any;

  constructor(private skeduloApiService: SkeduloApiService) {
      this.question1Sub = this.skeduloApiService.question1ResponseRx()
        .subscribe(response => {
          if(response){
            this.loadPage(response)
          }
        });
  }

  loadPage(response){
      this.loading = false;
      this.numbers = response;
  }

  getAnswer1(){
    this.loading = true;
    this.skeduloApiService.question1();
  }

  clearPage(){
    this.numbers = "";
  }
}
