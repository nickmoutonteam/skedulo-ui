export class OrderProduct {
  constructor(
  public orderId: number,
  public productId: number,
  public name?: string,
  public price?: number
) {}
}
