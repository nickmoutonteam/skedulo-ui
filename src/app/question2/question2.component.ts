import { Component, OnInit } from '@angular/core';
import { SkeduloApiService } from '../skedulo-api.service';
import { OrderProduct } from './order-product';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ghu-question2',
  templateUrl: './question2.component.html',
  styleUrls: ['./question2.component.scss']
})
export class Question2Component implements OnInit {
  loading: boolean = false;
  question2Sub: Subscription;
  orderProducts: OrderProduct[];
  joinedOrderProducts: OrderProduct[];

  constructor(private skeduloApiService: SkeduloApiService) {
    this.question2Sub = this.skeduloApiService.question2ResponseRx()
      .subscribe(response => {
        if(response){
          this.loadPage(response)
        }
      });
  }

  ngOnInit() {
  }

  loadPage(response){
      this.loading = false;
      this.joinedOrderProducts = response;
  }

  getAnswer2(){
    this.loading = true;
    const orderProduct1 = new OrderProduct(1,1);
    const orderProduct2 = new OrderProduct(2,2);
    this.orderProducts = Array.of(orderProduct1,orderProduct2)
    this.skeduloApiService.question2(this.orderProducts);
  }

}
