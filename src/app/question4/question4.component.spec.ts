import { TestBed, async } from '@angular/core/testing';
import { Question4Component } from './question1.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule }   from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { GithubApiService } from '../github-api.service';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HttpClientModule }    from '@angular/common/http';

describe('Question4Component', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        Question4Component
      ],
      imports: [
      MatProgressSpinnerModule,
      MatTableModule,
      MatInputModule,
      MatToolbarModule,
      MatCardModule,
      ReactiveFormsModule,
      HttpClientModule,
      BrowserAnimationsModule
    ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(Question4Component);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`loading should be false`, async(() => {
    const fixture = TestBed.createComponent(Question4Component);
    const app = fixture.debugElement.componentInstance;
    expect(app.loading).toEqual(false);
  }));
  it('should render the title bar', async(() => {
    const fixture = TestBed.createComponent(Question4Component);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('mat-hint').textContent).toContain('Enter a username to search github');
  }));
});
