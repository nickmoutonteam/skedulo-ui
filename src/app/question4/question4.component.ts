import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { GithubApiService } from '../github-api.service';
import { Subscription } from 'rxjs';
import { forwardRef } from '@angular/core';
import { debounceTime } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'ghu-question4',
  templateUrl: './question4.component.html',
  styleUrls: ['./question4.component.scss']
})
export class Question4Component implements OnInit {
  githubResponseSub: Subscription;
  userForm = new FormGroup ({});
  users: any;
  displayedColumns = ['avatar_url', 'login', 'type', 'score'];
  loading: boolean = false;
  rateLimited: boolean = false;

  constructor(private gitHubApiService: GithubApiService,
    private fb: FormBuilder) {
      this.githubResponseSub = this.gitHubApiService.gitHubResponseRx()
        .subscribe(response => {
          if (response == "chill") {
            this.clearPage();
            this.rateLimited = true;
            this.loading = false;
          } else {
            this.loadPage(response);
          }
        });
  }

  ngOnInit() {
    this.userForm = this.fb.group({
      index: [{value: null, disabled:true}],
      userName : []
    });

    this.onChanges();
  }

  onChanges(): void {
    this.userForm.get('userName').valueChanges
    .pipe(
      debounceTime(1000)
    ).subscribe(username => {
      if(username.length >= 3) {
        this.loading = true;
        this.gitHubApiService.searchUsers(username);
      } else if (username.length == 0) {
        this.clearPage();
      }
    });;
  }

  loadPage(response){
      this.clearPage();
      this.loading = false;
      this.users = new MatTableDataSource(response.items);
  }

  clearPage(){
    this.users = null;
  }

}
