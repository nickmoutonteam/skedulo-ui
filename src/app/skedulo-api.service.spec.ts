import { TestBed, inject } from '@angular/core/testing';

import { SkeduloApiService } from './skedulo-api.service';

describe('SkeduloApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SkeduloApiService]
    });
  });

  it('should be created', inject([SkeduloApiService], (service: SkeduloApiService) => {
    expect(service).toBeTruthy();
  }));
});
