import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient,HttpParams,HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { OrderProduct } from './question2/order-product';

@Injectable({
  providedIn: 'root'
})
export class SkeduloApiService {
  private skeduleBooAppUrl = environment.skeduleBooAppUrl;
  private question1Source = new Subject<any>();
  private question2Source = new Subject<any>();
  private bonusQuestionSource = new Subject<any>();

  constructor(private http: HttpClient) {
  }

  question1ResponseRx(): Observable<any> {
    return this.question1Source.asObservable();
  }

  question2ResponseRx(): Observable<any> {
    return this.question2Source.asObservable();
  }

  bonusQuestionResponseRx(): Observable<any> {
    return this.bonusQuestionSource.asObservable();
  }


  question1(){
    return this.http
      .get(this.skeduleBooAppUrl+'/api/question1')
      .subscribe(resp => {
        this.question1Source.next(resp);
      });
  }

  question2(orderProduct: OrderProduct[]){
    return this.http
      .post(this.skeduleBooAppUrl+'/api/question2',orderProduct)
      .subscribe(resp => {
        this.question2Source.next(resp);
      });
  }

  bonusQuestion(){
    return this.http
      .get(this.skeduleBooAppUrl+'/api/bonusQuestion')
      .subscribe(resp => {
        this.bonusQuestionSource.next(resp);
      });
  }

}
