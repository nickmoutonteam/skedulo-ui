export const environment = {
  production: true,
  prefix: 'prod',
  baseGithubUrl: 'https://api.github.com'
};
